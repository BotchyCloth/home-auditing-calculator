package home.auditing.calculator;

import java.awt.GridLayout;

import javax.swing.BorderFactory;
import javax.swing.JCheckBox;
import javax.swing.JPanel;

public class CheckBoxPanel extends JPanel {

	public CheckBoxPanel() {

		// Set border and title for this panel
		setBorder(BorderFactory.createTitledBorder("Select Appliances"));

		// Set layout using a grid of 4 rows 2 columns
		setLayout(new GridLayout(4, 2, 5, 10));

		// CheckBox Initialization
		appliance1 = new JCheckBox("Appliance 1");
		appliance2 = new JCheckBox("Appliance 2");
		appliance3 = new JCheckBox("Appliance 3");
		appliance4 = new JCheckBox("Appliance 4");
		appliance5 = new JCheckBox("Appliance 5");
		appliance6 = new JCheckBox("Appliance 6");
		appliance7 = new JCheckBox("Appliance 7");

		// Layout components on the panel
		add(appliance1);
		add(appliance2);
		add(appliance3);
		add(appliance4);
		add(appliance5);
		add(appliance6);
		add(appliance7);
	}

	// Return user selection to controller
	public boolean appliance1IsSelected() {
		return appliance1.isSelected();
	}

	// Return user selection to controller
	public boolean appliance2IsSelected() {
		return appliance2.isSelected();
	}

	// Return user selection to controller
	public boolean appliance3IsSelected() {
		return appliance3.isSelected();
	}

	// Return user selection to controller
	public boolean appliance4IsSelected() {
		return appliance4.isSelected();
	}

	// Return user selection to controller
	public boolean appliance5IsSelected() {
		return appliance5.isSelected();
	}

	// Return user selection to controller
	public boolean appliance6IsSelected() {
		return appliance6.isSelected();
	}

	// Return user selection to controller
	public boolean appliance7IsSelected() {
		return appliance7.isSelected();
	}

	private static final long serialVersionUID = 0X101L;

	private final JCheckBox appliance1;
	private final JCheckBox appliance2;
	private final JCheckBox appliance3;
	private final JCheckBox appliance4;
	private final JCheckBox appliance5;
	private final JCheckBox appliance6;
	private final JCheckBox appliance7;

}
