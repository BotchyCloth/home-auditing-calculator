package home.auditing.calculator;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.text.NumberFormat;
import java.util.Locale;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import home.auditing.calculator.Application;
import home.auditing.calculator.GUI;
import home.auditing.calculator.CheckBoxPanel;
import home.auditing.calculator.TotalsPanel;
import home.auditing.calculator.UserInputPanel;

public class Controller {

	public static void main(String[] args) {
		Controller controller = new Controller();
		controller.gui = new GUI();
		controller.app = new Application();
		controller.initializeActionListeners();
	}

	private void initializeActionListeners() {
		kwhFieldActionListener();
		hoursFieldActionListener();
		gallonsFieldActionListener();
		checkBoxPanelActionListener();
		exitButtonActionListener();
	}

	// kwhField action listener
	private void kwhFieldActionListener() {

		UserInputPanel userInputPanel = (UserInputPanel) gui.getPanels()[0];

		userInputPanel.getComponent(1).addFocusListener(new FocusListener() {

			@Override
			public void focusLost(FocusEvent e) {

				try {
					// Parse input from kwhField to double value
					double userInput = Double.parseDouble(userInputPanel
							.getKwhFieldText());

					// Check to see if value has changed and make update
					if (cacheKwh != userInput) {
						cacheKwh = userInput;
						app.setKwh(cacheKwh);
						total = app.getTotal();
						if (total > 0)
							updateTotalsPanel();
					}
				} catch (NumberFormatException nfe) {
					if (userInputPanel.getKwhFieldText().equals(""))
						return;
					else
						JOptionPane.showMessageDialog(gui,
								userInputPanel.getKwhFieldText()
										+ " is invalid input");
				}
			}

			@Override
			public void focusGained(FocusEvent e) {
				((JTextField) userInputPanel.getComponent(1)).setText("");
			}
		});
	}

	// hoursField action listener
	private void hoursFieldActionListener() {

		UserInputPanel userInputPanel = (UserInputPanel) gui.getPanels()[0];

		userInputPanel.getComponent(3).addFocusListener(new FocusListener() {

			@Override
			public void focusLost(FocusEvent e) {

				try {
					// Parse input from hoursField to double value
					double userInput = Double.parseDouble(userInputPanel
							.getHoursFieldText());

					// Check to see if value has changed and make update
					if (cacheHours != userInput) {
						cacheHours = userInput;
						app.setHours(cacheHours);
						total = app.getTotal();
						if (total > 0)
							updateTotalsPanel();
					}
				} catch (NumberFormatException nfe) {
					if (userInputPanel.getHoursFieldText().equals(""))
						return;
					else
						JOptionPane.showMessageDialog(gui,
								userInputPanel.getHoursFieldText()
										+ " is invalid input");
				}
			}

			@Override
			public void focusGained(FocusEvent e) {
				((JTextField) userInputPanel.getComponent(3)).setText("");
			}
		});
	}

	// gallonsField action listener
	private void gallonsFieldActionListener() {

		UserInputPanel userInputPanel = (UserInputPanel) gui.getPanels()[0];

		userInputPanel.getComponent(5).addFocusListener(new FocusListener() {

			@Override
			public void focusLost(FocusEvent e) {

				try {
					// Parse input from gallonsField to double value
					double userInput = Double.parseDouble(userInputPanel
							.getGallonsFieldText());

					// Check to see if value has changed and make update
					if (cacheGallons != userInput) {
						cacheGallons = userInput;
						app.setGallons(cacheGallons);
						total = app.getTotal();
						if (total > 0)
							updateTotalsPanel();
					}
				} catch (NumberFormatException nfe) {
					if (userInputPanel.getGallonsFieldText().equals(""))
						return;
					else
						JOptionPane.showMessageDialog(gui,
								userInputPanel.getGallonsFieldText()
										+ " is invalid input");
				}
			}

			@Override
			public void focusGained(FocusEvent e) {
				((JTextField) userInputPanel.getComponent(5)).setText("");
			}
		});
	}

	private void checkBoxPanelActionListener() {

		CheckBoxPanel checkBoxPanel = (CheckBoxPanel) gui.getPanels()[1];

		// Appliance 1 event handler
		((JCheckBox) checkBoxPanel.getComponent(0))
				.addActionListener(new ActionListener() {

					@Override
					public void actionPerformed(ActionEvent e) {
						if (((JCheckBox) checkBoxPanel.getComponent(0))
								.isSelected()) {
							total += 10.0;
							updateTotalsPanel();
						} else {
							total -= 10.0;
							updateTotalsPanel();
						}
					}
				});

		// Appliance 2 event handler
		((JCheckBox) checkBoxPanel.getComponent(1))
				.addActionListener(new ActionListener() {

					@Override
					public void actionPerformed(ActionEvent e) {
						if (((JCheckBox) checkBoxPanel.getComponent(1))
								.isSelected()) {
							total += 20.0;
							updateTotalsPanel();
						} else {
							total -= 20.0;
							updateTotalsPanel();
						}
					}
				});

		// Appliance 3 event handler
		((JCheckBox) checkBoxPanel.getComponent(2))
				.addActionListener(new ActionListener() {

					@Override
					public void actionPerformed(ActionEvent e) {
						if (((JCheckBox) checkBoxPanel.getComponent(2))
								.isSelected()) {
							total += 30.0;
							updateTotalsPanel();
						} else {
							total -= 30.0;
							updateTotalsPanel();
						}
					}
				});

		// Appliance 4 event handler
		((JCheckBox) checkBoxPanel.getComponent(3))
				.addActionListener(new ActionListener() {

					@Override
					public void actionPerformed(ActionEvent e) {
						if (((JCheckBox) checkBoxPanel.getComponent(3))
								.isSelected()) {
							total += 40.0;
							updateTotalsPanel();
						} else {
							total -= 40.0;
							updateTotalsPanel();
						}
					}
				});

		// Appliance 5 event handler
		((JCheckBox) checkBoxPanel.getComponent(4))
				.addActionListener(new ActionListener() {

					@Override
					public void actionPerformed(ActionEvent e) {
						if (((JCheckBox) checkBoxPanel.getComponent(4))
								.isSelected()) {
							total += 50.0;
							updateTotalsPanel();
						} else {
							total -= 50.0;
							updateTotalsPanel();
						}
					}
				});

		// Appliance 6 event handler
		((JCheckBox) checkBoxPanel.getComponent(5))
				.addActionListener(new ActionListener() {

					@Override
					public void actionPerformed(ActionEvent e) {
						if (((JCheckBox) checkBoxPanel.getComponent(5))
								.isSelected()) {
							total += 60.0;
							updateTotalsPanel();
						} else {
							total -= 60.0;
							updateTotalsPanel();
						}
					}
				});

		// Appliance 7 event handler
		((JCheckBox) checkBoxPanel.getComponent(6))
				.addActionListener(new ActionListener() {

					@Override
					public void actionPerformed(ActionEvent e) {
						if (((JCheckBox) checkBoxPanel.getComponent(6))
								.isSelected()) {
							total += 70.0;
							updateTotalsPanel();
						} else {
							total -= 70.0;
							updateTotalsPanel();
						}
					}
				});
	}

	// Method to update totals panel
	private void updateTotalsPanel() {

		TotalsPanel totalsPanel = (TotalsPanel) gui.getPanels()[2];

		totalsPanel.setDisplayText(String.format("Kw/H: %.1f%n%n"
				+ "Hours: %.1f%n%n" + "Gallons: %.1f%n%n"
				+ "Total (%.2f * %.2f + %.2f): %s", app.getKwh(), app
				.getHours(), app.getGallons(), app.getKwh(), app.getHours(),
				app.getGallons(), NumberFormat.getCurrencyInstance(Locale.US)
						.format(total)));
	}

	// exitButton action listener
	private void exitButtonActionListener() {

		((JButton) gui.getPanels()[3].getComponent(0))
				.addActionListener(new ActionListener() {

					@Override
					public void actionPerformed(ActionEvent e) {
						System.exit(0x0);
					}
				});
	}

	private GUI gui;
	private Application app;

	private double cacheKwh;
	private double cacheHours;
	private double cacheGallons;

	private double total;

}
