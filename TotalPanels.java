package home.auditing.calculator;

import java.awt.Color;

import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.JTextArea;

public class TotalsPanel extends JPanel {

	public TotalsPanel() {

		// Set border and title for this panel
		setBorder(BorderFactory.createTitledBorder("Totals"));

		// Initialize display
		display = new JTextArea("", 9, 38);
		
		// Configure display properties
		display.setFocusable(false);
		display.setBackground(Color.black);
		display.setForeground(Color.green);

		// Add display to panel
		add(display);
	}

	// Receives information from controller to update display
	public void setDisplayText(final String result) {
		display.replaceRange(result, 0, display.getText().length());
	}

	private static final long serialVersionUID = 0X010L;

	private final JTextArea display;

}

UserInputPanel.JAVA Class File 
package home.auditing.calculator;

import java.awt.GridLayout;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class UserInputPanel extends JPanel {

	public UserInputPanel() {

		// Set border and title for this panel
		setBorder(BorderFactory.createTitledBorder("User Input"));

		// Set layout using a grid of 3 rows 2 column
		setLayout(new GridLayout(3, 2));

		// Labels Initialization
		kwhLabel = new JLabel("Kw/h:");
		hoursLabel = new JLabel("Hours:");
		gallonsLabel = new JLabel("Gallons:");

		// Fields Initialization
		kwhField = new JTextField(null, 15);
		hoursField = new JTextField(null, 15);
		gallonsField = new JTextField(null, 15);

		// Layout components on the panel
		add(kwhLabel);
		add(kwhField);
		add(hoursLabel);
		add(hoursField);
		add(gallonsLabel);
		add(gallonsField);
	}

	// Return user input to controller
	public String getKwhFieldText() {
		return kwhField.getText();
	}

	// Return user input to controller
	public String getHoursFieldText() {
		return hoursField.getText();
	}

	// Return user input to controller
	public String getGallonsFieldText() {
		return gallonsField.getText();
	}

	private static final long serialVersionUID = 0X111L;

	// Labels Declarations
	private final JLabel kwhLabel;
	private final JLabel hoursLabel;
	private final JLabel gallonsLabel;

	// Fields Declarations
	private final JTextField kwhField;
	private final JTextField hoursField;
	private final JTextField gallonsField;

}
