package home.auditing.calculator;

import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import java.util.ArrayList;


import home.auditing.calculator.CheckBoxPanel;
import home.auditing.calculator.TotalsPanel;
import home.auditing.calculator.UserInputPanel;

public class GUI extends JFrame {

	public GUI() {

		// Set title for this frame
		setTitle("Home Utility Auditing Calculator");

		// Set this frame's layout to null (Absolute positioning)
		setLayout(null);
		
		// Set closing behavior on this frame
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		// Initialize panels
		userInputPanel = new UserInputPanel();
		checkBoxPanel = new CheckBoxPanel();
		totalsPanel = new TotalsPanel();
		exitButtonPanel = new JPanel();
		
		// Initialize exit button and add to button panel
		exitButton = new JButton("Exit");
		exitButtonPanel.add(exitButton);

		// Position panels
		userInputPanel.setBounds(5, 5, 280, 170);
		checkBoxPanel.setBounds(290, 5, 280, 170);
		totalsPanel.setBounds(576, 5, 434, 170);
		exitButtonPanel.setBounds(5, 175, 1024, 30);

		// Add panels to this frame
		getContentPane().add(userInputPanel);
		getContentPane().add(checkBoxPanel);
		getContentPane().add(totalsPanel);
		getContentPane().add(exitButtonPanel);

		// Package all panels for distribution to controller
		panels = new JPanel[] { userInputPanel, checkBoxPanel, totalsPanel,
				exitButtonPanel };

		// Configure this frame's properties
		setSize(1024, 240);
		setResizable(false);
		setLocationRelativeTo(null);
		setVisible(true);
	}

	// Panels package distributor
	public JPanel[] getPanels() {
		return panels;
	}

	private static final long serialVersionUID = 0X100L;

	private final UserInputPanel userInputPanel;
	private final CheckBoxPanel checkBoxPanel;
	private final TotalsPanel totalsPanel;

	private final JPanel exitButtonPanel;
	private final JButton exitButton;

	private final JPanel[] panels;

}
