package home.auditing.calculator;

public class Application {

	public double getHours() {
		return hours;
	}

	public void setHours(final double hours) {
		this.hours = hours;
	}

	public double getGallons() {
		return gallons;
	}

	public void setGallons(final double gallons) {
		this.gallons = gallons;
	}

	public double getKwh() {
		return kwh;
	}

	public void setKwh(final double kwh) {
		this.kwh = kwh;
	}

	public double getTotal() {
		return ( (kwh * hours) + (gallons * PRICE_PER_GALLON) );
	}
	
	private final double PRICE_PER_GALLON = 1.99;

	private double kwh;
	private double hours;
	private double gallons;

}
